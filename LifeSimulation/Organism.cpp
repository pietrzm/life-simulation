#include "stdafx.h"
#include "Organism.h"

Organism::Organism(unsigned int x, unsigned int y, Race rc, char sprite, Map * map, unsigned int lp):
	GameObject(x, y, sprite, map), race(rc), lifePoints(lp)
{
	/*race = rc;
	lifePoints = lp;*/

	map->addOrganism(this);
	map->addOrganismOnMap(this);

}


Organism::~Organism()
{
}



Race Organism::getRace()
{
	return race;
}

int Organism::getLifePoints()
{
	return lifePoints;
}

void Organism::changeLifePoints(int dLP)
{
	lifePoints += dLP;
}

void Organism::eat(Organism * organism)
{
}

bool Organism::checkXY(Coordinate move)
{

	if ((this->getX() + move.x) < map->getWidth() && (this->getX() + move.x) >= 0 && (this->getY() + move.y) < map->getHeight() && (this->getY() + move.y) >= 0) {
		return true;
	}
	else
	{
		return false;
	}

}