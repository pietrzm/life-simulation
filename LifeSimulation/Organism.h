#pragma once
#include "stdafx.h"

class Map;
//class GameObject;

class Organism : public GameObject
{
public:
		 
	Organism(unsigned int x, unsigned int y, Race rc, char sprite, Map * map, unsigned int lp);
	virtual ~Organism();
		
	Race getRace();
	
	void changeLifePoints(int dLP);
	int getLifePoints();

	void act() = 0;

	virtual void eat(Organism* organism);

private: 
	
	Race race;
	int lifePoints;

	virtual void moveOrganism() = 0;
	virtual void eat() = 0;
	

protected:
	
	bool checkXY(Coordinate move); //moze w GameObject badz w mapie >?
	
	vector <Coordinate> moveBuffer;
	
};

