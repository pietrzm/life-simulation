#pragma once



class Map;
class Field;

class GameObject : public IObservable, public NonCopyable<GameObject>
{
public:
	GameObject(unsigned int x, unsigned int y, char sprite, Map * map);
	virtual ~GameObject();
	
	void act();
	void render();
	
	int getId();

	int getX();
	void setX(unsigned int x);

	int getY();
	void setY(unsigned int y);

	char getSprite();
	void setSprite(char sprite);

	Field * getField();
	void setField(Field * field);
	//nie jestem pewien czy bede z tego korzystal

	static int mapWidth;
	static int mapHeight;
	//po co to?

	bool getAlive();
	void kill();


private:
	
	bool alive = true;

	unsigned int id;
	
	unsigned int x, y;

	char sprite;
	
	Field * field;

	void generateId();



protected:
	
	GameObject * findNearest(GameObject * gameObject);
	GameObject * findFarthest(GameObject * gameObject);
	int findDistance(GameObject * gameObject);
	bool collisionWith(GameObject * gameObject);

	Map * map;

	//implementacja tych metod wymagala by przekazywania do gameobjectu wskaznika na mape badz bezposrednio plansze
	//wg mnie takie metody operujace na strukturach mapy powinny byc w mapy, w tym scopie tylko je wywolujmy 
	
};

