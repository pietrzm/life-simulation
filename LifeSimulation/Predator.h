#pragma once
#include "stdafx.h"


class Predator :
	public Organism
{
public:
	Predator(unsigned int x, unsigned int y, Race rc, char sprite, Map * map, unsigned int lp, unsigned int iq);
	~Predator();
	
	int getIQ();//private? albo niepotrzebny wg

	void act();
	void moveOrganism();
	void eat();
	void eat(Organism* organism);
	

private :
	unsigned int IQ;

	void initializeMoveBuffer();
	void seekPrey();

};

