#include "stdafx.h"
#include <algorithm>
#include "Predator.h"


Predator::Predator(unsigned int x, unsigned int y, Race rc, char sprite, Map * map, unsigned int lp, unsigned int iq) :
	Organism (x, y, rc, sprite, map, lp), IQ(iq)
{
		
}


Predator::~Predator()
{
	
}

int Predator::getIQ()
{
	return IQ;

}

void Predator::act()
{
	moveOrganism();
	eat();

}

void Predator::moveOrganism() {
	
	initializeMoveBuffer();

	seekPrey();
	
	if (!moveBuffer.empty()) {

		Coordinate move = moveBuffer[rand() % moveBuffer.size()];

		map->moveOrganism(this, move);

	}

	moveBuffer.clear();

}

void Predator::eat()
{

	Coordinate field(this->getX(), this->getY());

	Organism* victim = map->seekOrganism(field, PREY);

	if (victim != nullptr) {
		this->changeLifePoints(victim->getLifePoints());
		map->killOrganism(victim);
	}
	else
	{
		this->changeLifePoints(-1);
		
		if (this->getLifePoints()==0)
		{
			map->killOrganism(this);
		}
	}

}

void Predator::eat(Organism* organism) {

	Coordinate field(this->getX(), this->getY());
	this->changeLifePoints(organism->getLifePoints());
	map->killOrganism(organism);

}

void Predator::initializeMoveBuffer() {

	Coordinate move(0,0);

	for (int i = -1; i <= 1; i++)
	{
		if (i != 0) {
			move.x = i * 2;
			for (int a = -1; a <= 1; a++)
			{
				if (a != 0) {
					move.y = a;
					if (checkXY(move))
					{
						move.x += this->getX();
						move.y += this->getY();
						moveBuffer.push_back(move);

					}
				}

			}
		}
		else {
			for (int j = -1; j <= 1; j++)
			{
				if (j != 0) {
					move.y = j * 2;
					for (int b = -1; b <= 1; b++)
					{
						if (b != 0) {
							move.x = b;
							if (checkXY(move))
							{
								move.x += this->getX();
								move.y += this->getY();
								moveBuffer.push_back(move);

							}
						}

					}
				}
			}
		}

	}

	random_shuffle(moveBuffer.begin(), moveBuffer.end());

}

void Predator::seekPrey()
{
	int currentIQ = this->getIQ();

	vector <Coordinate>::iterator move = moveBuffer.begin();
	while (move != moveBuffer.end() && moveBuffer.size() > 1)
	{
		if (map->seekOrganism(*move, PREY) == nullptr && currentIQ>0) {
			move = moveBuffer.erase(move);
			currentIQ--;
		}
		else {
			++move;
		}
	}
	
}

