#pragma once
#include "stdafx.h"
//

class Prey :
	public Organism
{
public:
	Prey(unsigned int x, unsigned int y, Race rc, char sprite, Map * map, unsigned int lp);
	~Prey();

	void act();
	void moveOrganism();
	void eat();

private:
	void initializeMoveBuffer();
};

