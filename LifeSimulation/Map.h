#pragma once
#include "Matrix.h"

enum Race;
class Field;
class Organism;


class Map final /*: public IObservable*/ //metody act i render w mapie?!
{
public:
	Map(int width, int height);
	virtual ~Map();
	
	void update(int steps);
	void render();

	int getWidth();
	int getHeight();

	Matrix<Field> getFields();
	
	list<Organism*> * getOrganisms();
	
	void addOrganism(Organism * organism);

	void moveOrganism(Organism * organism, Coordinate move);
	void killOrganism(Organism * organism);

	void organismsGarbageCollector();

	Organism* seekOrganism(Coordinate field, Race race);

	void addOrganismOnMap(Organism * organism);
	void addTeleporterOnMap(Teleporter * teleporter);

private:

	int width, height; 
	Matrix<Field> fields;
	
	list<Organism*> organisms;
	
};

